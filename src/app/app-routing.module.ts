import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DigimonHomeComponent } from './digimon-home/digimon-home.component';
import { DigimonMainComponent } from './digimon-main/digimon-main.component';


const routes: Routes = [
  {path: 'home', component: DigimonMainComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
