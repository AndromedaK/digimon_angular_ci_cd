import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Digimons} from "src/interfaces";

@Component({
  selector: "app-digimon-header",
  templateUrl: "./digimon-header.component.html",
  styleUrls: ["./digimon-header.component.scss"],
})
export class DigimonHeaderComponent implements OnInit {
  @Output() searchChange = new EventEmitter();
  @Output() levelSelected = new EventEmitter();

  //Levels: string;
  DigimonList: Digimons[]
  search: string;
  levels: Array<string>;
  currentLevel: string;

  @Input() set digimons(digimons: Digimons[]) {
    if (digimons !== this.DigimonList) {
      this.DigimonList = digimons;
      this.setDigimonLevels(this.DigimonList)     
    }
  }

  constructor() {

  }

  ngOnInit(){
    // cuando cargue la pagina vienen vacios
    this.levels = [];
  }

  searchEvent(search): void {
    if (search === '') {
      this.search = search;
    }
    this.searchChange.emit(this.search);
  }

  
  onLevelSelected(): void {
    if (this.currentLevel) {
      this.levelSelected.emit(this.currentLevel);
    } else {
      this.levelSelected.emit('');
    }
  }



  setDigimonLevels(digimons: Digimons[]): void {
    digimons.forEach( digimon => {
      const LEVEL = digimon.level;
      if (!this.levels.includes(LEVEL)) {
        this.levels.push(LEVEL);
        this.levels.sort();
      }
    });
  }
}
