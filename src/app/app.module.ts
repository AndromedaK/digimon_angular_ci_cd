import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialAngularModule } from './modules/material-angular/material-angular.module';
import { SearchPipe } from './pipes/search.pipe';
import { NivelPipe } from './pipes/nivel.pipe';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DigimonService }  from './Servicios/digimon.service';
import { DigimonHeaderComponent } from './digimon-header/digimon-header.component';
import { DigimonHomeComponent } from './digimon-home/digimon-home.component';
import { DigimonMainComponent } from './digimon-main/digimon-main.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchPipe,
    NivelPipe,
    DigimonHeaderComponent,
    DigimonHomeComponent,
    DigimonMainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialAngularModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
