import { Component, OnInit, EventEmitter,Input,Output } from '@angular/core';
import {Digimons} from 'src/interfaces';
import {DigimonService} from 'src/app/Servicios/digimon.service';

@Component({
  selector: 'app-digimon-home',
  templateUrl: './digimon-home.component.html',
  styleUrls: ['./digimon-home.component.scss']
})
export class DigimonHomeComponent implements OnInit {

  @Output() exportDigimons = new EventEmitter();
  digimonLoaded = false;
  digimons: Digimons[];
  query: string;
  levelsFilter: string;

  @Input() set search(newSearch: string) {
    if (newSearch !== this.query) {
      this.query = newSearch;
    }
  }

  @Input() set levelFilter(levelFilter: string) {
    if (levelFilter !== this.levelsFilter) {
        this.levelsFilter = levelFilter;
    }
  }
  constructor(private digimonService: DigimonService) { }

  ngOnInit(){

  this.digimonLoaded = false;
  this.getDigimons();


  }


  getDigimons(): void {
    this.digimonService.getDigimon().subscribe((data: Digimons[]) => {
      this.digimons = data;
      this.exportDigimons.emit(this.digimons);
    });
  }



}
