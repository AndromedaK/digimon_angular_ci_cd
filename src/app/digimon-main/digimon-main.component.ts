import { Component, OnInit } from '@angular/core';
import { Digimons } from 'src/interfaces';

@Component({
  selector: 'app-digimon-main',
  templateUrl: './digimon-main.component.html',
  styleUrls: ['./digimon-main.component.scss']
})
export class DigimonMainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public search: string;
  public digimons:  Digimons[];
  public levelFilter : string;

  exportDigimons(digimons: Digimons[]) {
    if (this.digimons !== digimons) {
       this.digimons = digimons;
    }
  }

  newDigimonSearch(search: string): void {
    if (this.search !== search) {
      this.search = search;
    }
  }

  newLevelSelected (level: string): void {
    if (this.levelFilter !== level) {
        this.levelFilter = level;
    }
  }

}
