import { Injectable } from "@angular/core";
import { Digimons} from "src/interfaces";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";

@Injectable()
export class ServiceNameService {
  constructor(private httpClient: HttpClient) {}
}

@Injectable({
  providedIn: "root",
})

// Los servicios seran nuestros proveedores de funcionalidades
// Aqui estará toda la Data que nuestros componentes van a extraer
// Por medio de estos servicios pondrán maniuzpular la Data
export class DigimonService {
  Digimons: any;

  // En el constructor podemos cargar dat cuando se inicializa nuestro componente
  constructor(private http: HttpClient) {
    this.Digimons = environment.DigimonURL;
  }

  /**
   *
   */

  /** Retorno todo los digimons */
  getDigimon(): Observable<Digimons[]> {
    return this.http
      .get<Digimons[]>(`${this.Digimons}`)
      .pipe(catchError(this._handleError));
  }

  private _handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
}
